package bai4_bai7_Streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TestStream {

        public static void main(String[] args) {
            //Stream filter
            System.out.println("--------Stream filter-------------");
            List<String> lines = Arrays.asList("Spring","Angular","Vuejs");

            List<String> result = lines.stream()
                    .filter(line -> !"Angular".equals(line))
                    .collect(Collectors.toList());

            result.forEach(System.out :: println);
            //End Stream filter


            //Get Person By name
            System.out.println("--------Get Person by Name-------------");
            List<Person> personList = Arrays.asList(
                    new Person("Spring",20),
                    new Person("Angular",21),
                    new Person("Vuejs",18)
            );

            Person findAngular = personList.stream()
                    .filter(n -> "Angular".equals(n.getName()))
                    .findAny()
                    .orElse(null);

            System.out.println(findAngular.toString());



            Person findSpring = personList.stream()
                    .filter(n -> "Spring".equals(n.getName()))
                    .findAny()
                    .orElse(null);

            System.out.println(findSpring.toString());

            //Multiple condition
            System.out.println();
            System.out.println("--------Multiple condition-------------");
            Person findAngularAndAge = personList.stream()
                    .filter(n -> "Angular".equals(n.getName()) && n.getAge() == 21)
                    .findAny()
                    .orElse(null);

            System.out.println(findAngularAndAge.toString());



            Person findSpringAndAge = personList.stream()
                    .filter(n -> "Spring".equals(n.getName()) && n.getAge() == 20)
                    .findAny()
                    .orElse(null);

            System.out.println(findSpringAndAge.toString());

            //Stream filter and map
            System.out.println("--------Stream filter and map -------------");
            String name = personList.stream()
                    .filter(x -> "Spring".equals(x.getName()))
                    .map(Person :: getName)
                    .findAny()
                    .orElse("");

            System.out.println("name: " + name);

            List<String> collect = personList.stream()
                    .map(Person::getName)
                    .collect(Collectors.toList());

            collect.forEach(System.out::println);
        }

}
