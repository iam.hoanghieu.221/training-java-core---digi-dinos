package bai8_flatMap;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TestExample2 {
    public static void main(String[] args) {
        int[] intArray = {1,2,3,4,5,6};

        Stream<int[]> arrStream = Stream.of(intArray);

        IntStream intStream = arrStream.flatMapToInt(x -> Arrays.stream(x));

        intStream.forEach(x -> System.out.println(x));
    }
}
