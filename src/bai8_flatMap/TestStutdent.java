package bai8_flatMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

// Stream  + set + flatMap
public class TestStutdent {
    public static void main(String[] args) {
        Student student = new Student();
        student.setName("Hieu");
        student.addBook("Spring");
        student.addBook("Angular");
        student.addBook("Vuejs");

        Student student2 = new Student();
        student2.setName("Linh");
        student2.addBook("Python");
        student2.addBook("C#");
        student2.addBook("C++");

        List<Student> list = Arrays.asList(student,student2);

        List<String> collectStrings =
                list.stream()
                            .map(x->x.getBook())
                            .flatMap(x->x.stream())
                            .distinct()
                            .collect(Collectors.toList());

        collectStrings.forEach(x -> System.out.println(x));
    }

}
