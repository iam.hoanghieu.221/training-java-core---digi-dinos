package bai8_flatMap;

import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.stream.Stream;

public class TestExample1 {
    public static void main(String[] args) {
        String[][] data = new String[][] {{"a","b"},{"c","d"},{"e","f"}};

        //Stream<String[]>
        Stream<String[]> temp = Arrays.stream(data);

        //Stream<String>
        Stream<String> stringStream = temp.flatMap(x -> Arrays.stream(x));

        //filter a stream of string[], and return a string[] ?
        Stream<String> stream = stringStream.filter(x -> !"".equals(x));
        stream.forEach(System.out:: println);
    }

}
