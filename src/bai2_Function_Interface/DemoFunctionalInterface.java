package bai2_Function_Interface;
@FunctionalInterface // Gắn cái này lên interface, nó đánh dấu interface chỉ được phép có 1 funtion thôi
public interface DemoFunctionalInterface {
    public String process(String input); // ham abstract duy nhat

    // Do đã có 1 hầm abstract rồi nên hàm này sẽ bị lỗi
//    public String processv2(String input);


    //Một class implement DemoFunctionalInterface đều
    // có thể Overide lại hàm này hoặc không
    public default void printf(Object t){

    }


}
