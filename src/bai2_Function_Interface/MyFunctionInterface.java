package bai2_Function_Interface;

@FunctionalInterface
public interface MyFunctionInterface {
    public void first();
//    public void doHomeWork(); => error
    default void doHomeWork(){

    }

    default void doHomeWork2(){

    }

}
