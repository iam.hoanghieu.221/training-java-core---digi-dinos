package bai9_10_11_12_13_14_15_16;

import bai4_bai7_Streams.Person;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Product {
    private Integer id;
    private String name;
    private Integer categoryId;
    private Date saleDate;
    private Integer quality;
    private boolean isDelete;

    public Product() {
    }

    public Product(Integer id, String name, Integer categoryId, Date saleDate, Integer quality, boolean isDelete) {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.saleDate = saleDate;
        this.quality = quality;
        this.isDelete = isDelete;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", categoryId=" + categoryId +
                ", saleDate=" + saleDate +
                ", quality=" + quality +
                ", isDelete=" + isDelete +
                '}';
    }

    // Bai 10
    public List<Product> createListProduct(){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            List<Product> lsProduct = Arrays.asList(
                    new Product(1,"Laptop Gaming MSI",1,dateFormat.parse("28/12/2019"),9000000,false),
                    new Product(2,"Iphone X",2,dateFormat.parse("20/12/2019"),30000000,false),
                    new Product(3,"Sumsung S10",2,dateFormat.parse("5/01/2020"),25000000,false),
                    new Product(4,"Chip Intel i9",3,dateFormat.parse("8/08/2019"),40000000,false),
                    new Product(5,"Quat tan nhiet",4,dateFormat.parse("24/12/2019"),400000,false),
                    new Product(6,"Laptop Gaming Dell AlienWare",1,dateFormat.parse("16/04/2019"),45000000,false),
                    new Product(7,"Sony Xpreria XA1 Plus",2,dateFormat.parse("11/01/2019"),6500000,false),
                    new Product(8,"NDIVIA 1050Ti",3,dateFormat.parse("21/08/2019"),9000000,false),
                    new Product(9,"De tan nhieu Laptop",4,dateFormat.parse("31/04/2019"),364000,false),
                    new Product(10,"NDIVIA 2080Ti",1,dateFormat.parse("28/02/2019"),22000000,false)

            );
            return lsProduct;
        }catch (Exception ex){
            ex.printStackTrace();
            return new ArrayList<>();
        }
    }

    // Bai 11
    public String getNameProductById(List<Product> lsProduct,Integer idProduct){
        //Cach 1
        String nameProduct = lsProduct.stream()
                .filter(x -> idProduct.equals(x.getId()))
                .map(Product :: getName)
                .findAny()
                .orElse("");

        //Cach 2
        String nameProduct2;
        for(Product product : lsProduct){
            if(product.getId().equals(idProduct)){
                nameProduct2 = product.getName();
            }
        }
//        return nameProduct2;
        return nameProduct;
    }

    // Bai 12
    public List<Product> filterProductByQuality(List<Product> lsProduct){
        //Cach 1
        List<Product> lsResultProduct = lsProduct.stream()
                .filter(x -> x.getQuality() > 0 && !x.isDelete())
                .collect(Collectors.toList());

        //Cach 2
        List<Product> lsResultProduct2 = new ArrayList<>();
        for(Product product : lsProduct){
            if(product.getQuality() > 0 && !product.isDelete()){
                lsResultProduct2.add(product);
            }
        }
//        return lsResultProduct2;
        return lsResultProduct;
    }

    // Bai 13
    public List<String> filterProductBySaleDate(List<Product> lsProduct){
        Date now = new Date();
        //Cach 1
        List<String> lsResultProduct = lsProduct.stream()
                .filter(x -> x.getSaleDate().compareTo(now)>0 && !x.isDelete())
                .map(Product::getName).collect(Collectors.toList());
        //Cach 2
        List<String> lsResultProduct2 = new ArrayList<>();
        for(Product product : lsProduct){
            if(product.getSaleDate().compareTo(now) > 0 && !product.isDelete()){
                lsResultProduct2.add(product.getName());
            }
        }
//        return lsResultProduct2;
        return lsResultProduct;
    }

    // Bai 14
    public Integer getTotalProduct(List<Product> lsProduct){
        //Cach 1
        Integer totalProduct = lsProduct.stream().filter(x->!x.isDelete())
                .map(Product::getQuality)
                .reduce(0,(element1, element2) -> element1 + element2);

        //Cach 2
        Integer totalProduct2 =0;
        for(Product obj : lsProduct){
            if(!obj.isDelete()){
                totalProduct2 += obj.getQuality();
            }
        }
//        return totalProduct2;
        return totalProduct;
    }

    // Bai 15
    public boolean isHaveProductInCategory(List<Product> lsProduct, Integer categoryId){
        boolean check = false;
        //Cach 1
        if(!lsProduct.stream().filter(x -> x.getCategoryId()  == categoryId).collect(Collectors.toList()).isEmpty()){
                check = true;
        }
        //Cach 2
        for(Product product : lsProduct){
            if(product.getCategoryId() == categoryId){
               check = true;
               break;
            }
        }
        return check;
    }

    // Bai 16
    public List<String[]> filterProductBySaleDateAndQuality(List<Product> lsProduct){
        Date now = new Date();
        //Cach 1
        List<String[]> arrayString = lsProduct.stream()
                .filter(x -> x.getSaleDate().compareTo(now) > 0 && x.getQuality() > 0)
                .map(key -> new String[]{key.getId().toString(), key.getName()})
                .collect(Collectors.toList());

        //Cach 2
        List<String[]> arrayString2 = new ArrayList<>();
        int row = 0;
        for (Product product2 : lsProduct) {
            if (product2.getSaleDate().compareTo(now) > 0 && product2.getQuality() > 0) {
                String arr[] = {product2.getId().toString(), product2.getName()};
                arrayString2.add(arr);
            }
        }

//        return arrayString2;
        return arrayString;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public Integer getQuality() {
        return quality;
    }

    public void setQuality(Integer qulity) {
        this.quality = qulity;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }
}
