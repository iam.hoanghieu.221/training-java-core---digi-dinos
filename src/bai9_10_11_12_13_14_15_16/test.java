package bai9_10_11_12_13_14_15_16;

import java.util.*;
import java.util.stream.Collectors;

public class test {
    public static void main(String[] args) {
        Date now = new Date();
        Product product = new Product();
        List<Product> lsProduct = product.createListProduct();

        //Test bai 12
        //C1
        List<Product> lsTest1 = lsProduct.stream()
                .filter(x -> x.getQuality() > 0 && !x.isDelete())
                .collect(Collectors.toList());
        lsTest1.forEach(x -> x.toString());

        //C2
        List<Product> lsTest2 = new ArrayList<>();
        for(Product obj : lsProduct){
            if(obj.getQuality() > 0 && !obj.isDelete()){
                lsTest2.add(obj);
            }
        }
        lsTest2.forEach(System.out::println);
        //Ket thuc test bai 12


        //Test bai 13
        //Cach 1
        List<String> lsTest4 = lsProduct.stream()
                .filter(x -> x.getSaleDate().compareTo(now)>0 && !x.isDelete())
                .map(Product::getName).collect(Collectors.toList());
        lsTest4.forEach(x -> x.toString());
        //Cach 2
        List<String> lsTest5 = new ArrayList<>();
        for(Product obj2 : lsProduct){
            if(obj2.getSaleDate().compareTo(now) > 0 && !obj2.isDelete()){
                lsTest5.add(obj2.getName());
            }
        }
        lsTest5.forEach(System.out::println);
        //Ket thuc test bai 13

        //Test bai 14
        //Cach 1
        Integer totalProduct = lsProduct.stream().filter(x->!x.isDelete())
                .map(Product::getQuality)
                .reduce(0,(element1, element2) -> element1 + element2);
        System.out.println(totalProduct);
        //Cach 2
        Integer totalProduct2 =0;
        for(Product obj : lsProduct){
            if(!obj.isDelete()){
                totalProduct2 += obj.getQuality();
            }
        }
        System.out.println(totalProduct2);
        //Ket thuc test bai 14


        //Test bai 15
        boolean check = false;
        int categoryId = 1;
        //Cach 1
        if(!lsProduct.stream().filter(x -> x.getCategoryId()  == categoryId).collect(Collectors.toList()).isEmpty()){
            check = true;
        }
        System.out.println(check);
        //Cach 2
        for(Product obj3 : lsProduct){
            if(obj3.getCategoryId() == categoryId){
                check = true;
                break;
            }
        }
        System.out.println(check);
        //Ket thuc test bai 15



        //Test bai 16

         List<String[]> arrayString = lsProduct.stream()
                .filter(x -> x.getSaleDate().compareTo(now) > 0 && x.getQuality() > 0)
                .map(key -> new String[]{key.getId().toString(), key.getName()})
                .collect(Collectors.toList());

        for(String[] a : arrayString){
            System.out.println(a[0] + " " + a[1]);
        }

        List<String[]> arrayString2 = new ArrayList<>();
        int row = 0;
        for (Product product2 : lsProduct) {
            if (product2.getSaleDate().compareTo(now) > 0 && product2.getQuality() > 0) {
                String arr[] = {product2.getId().toString(), product2.getName()};
                arrayString2.add(arr);
            }
        }

        for(String[] a : arrayString2){
            System.out.println(a[0] + " " + a[1]);
        }
        //Ket thuc Test bai 16
    }
}
