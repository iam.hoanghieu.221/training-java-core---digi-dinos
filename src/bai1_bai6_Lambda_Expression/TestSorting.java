package bai1_bai6_Lambda_Expression;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class TestSorting {
    public static void main(String[] args) {
        List<Developer> listDevs = getDeveloper();

        System.out.println("Before Sort");
        for(Developer developer : listDevs){
            System.out.println(developer);
        }
        System.out.println("After Sort");

        listDevs.sort((Developer o1, Developer o2) -> o1.getAge() - o2.getAge());
        listDevs.forEach((developer)-> System.out.println(developer));

    }


    private static List<Developer> getDeveloper(){
        List<Developer> result = Arrays.asList(
                    new Developer("hieu", new BigDecimal("20000"), 30),
                    new Developer("linh", new BigDecimal("22000"), 25),
                    new Developer("manh", new BigDecimal("32000"), 33),
                    new Developer("cuong", new BigDecimal("37000"), 38));

        return result;
    }

}
