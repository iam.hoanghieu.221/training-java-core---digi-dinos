package bai1_bai6_Lambda_Expression;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;

public class LambdaExpression {

    public static void main(String[] args) {

        Processor stringProcessor = (String str) -> str.length();
        String name = "Java Lambda";
        int length = stringProcessor.getStringLength(name);
        System.out.println(length);
        System.out.println(isPrime(2));
        System.out.println(findSquareOfMaxOdd(Arrays.asList(3,4,5,6,7,8,9,10,11)));
    }

    //kiem tra co phai so nguyen to hay khong
    public static boolean isPrime(int number) {
        return number > 1
                && IntStream.range(2, number).noneMatch(
                index -> number % index == 0);
    }

    //Tim ra so le lon nhat trong day so tu 3 -> 11
    public static int findSquareOfMaxOdd(List<Integer> numbers) {
        return numbers.stream()
                .filter(LambdaExpression::isOdd)
                .filter(LambdaExpression::isGreaterThan3)
                .filter(LambdaExpression::isLessThan11)
                .max(Comparator.naturalOrder())
                .map(i -> i * i)
                .get();
    }

    public static boolean isOdd(int i){
        return i % 2 != 0;
    }

    public static boolean isGreaterThan3(int i){
        return i > 3;
    }

    public static boolean isLessThan11(int i){
        return i < 11;
    }

    interface Processor {
        int getStringLength(String str);
    }

}
