package bai3_Default_Methods;

interface TestInterface{
    //abstract method
    public void square(int a);

    //default method
    default void show(){
        System.out.println("Default Method");
    }

    //default method
    static void show2() {
        System.out.println("Static Method");
    }

    }

public class TestClass  implements TestInterface{
    @Override
    public void square(int a){
        System.out.println(a*a);
    }

    public static void main(String[] args){
        TestClass testClass = new TestClass();
        testClass.square(4);

        //default method executed
        testClass.show();

        //static method excuted
        TestInterface.show2();
    }
}
