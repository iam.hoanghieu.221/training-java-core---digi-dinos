package bai3_Default_Methods;

interface TestInterface1 {
    //default method
    default void show() {
        System.out.println("Default Method TestInterface1");
    }
}

interface TestInterface2 {
    //Default method
    default void show() {
        System.out.println("Default Method TestInterface2");
    }
}

public class MultipleDefautMethods implements TestInterface1,TestInterface2 {

    public void show(){
        // dung ham cua TestInterface1
        TestInterface1.super.show();

        // dung ham cua TestInterface2
        TestInterface2.super.show();
    }

    public static void main(String[] args) {
        MultipleDefautMethods d = new MultipleDefautMethods();
        d.show();
    }


}
