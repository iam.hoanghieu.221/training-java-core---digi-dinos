package bai5_DateTime;

public class JobTimeDemo {
    private static int sum(){
        int sum = 0;
        for(int i = 0; i< 100; i++){
            sum += i;
        }
        return sum;
    }

    private static void doJob(int count){
        for(int i=0;i < count ; i++){
            sum();
        }
    }

    public static void main(String[] args) {
        long millis1 = System.currentTimeMillis();
        doJob(10000);
        long millis2 = System.currentTimeMillis();

        long distance = millis2 - millis1;

        System.out.println("Khoang cach thoi gian tinh bang milli giay: " + distance );
    }
}
