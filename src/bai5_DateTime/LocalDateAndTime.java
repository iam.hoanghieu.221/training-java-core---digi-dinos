package bai5_DateTime;

import java.time.LocalTime;

public class LocalDateAndTime {
    public static void main(String[] args) {
        // Local Date
        java.time.LocalDate localDate = java.time.LocalDate.now();
        System.out.println(localDate.toString());
        System.out.println(localDate.getDayOfWeek().toString());
        System.out.println(localDate.getDayOfMonth());
        System.out.println(localDate.getDayOfYear());
        System.out.println(localDate.isLeapYear());
        System.out.println(localDate.plusDays(12).toString());

        System.out.println();
        System.out.println("--------------Local Time-------------------");
        LocalTime localTime = LocalTime.now();
//        LocalTime localTime = LocalTime.of(12, 20);
        System.out.println(localTime.toString());
        System.out.println(localTime.getHour());
        System.out.println(localTime.getMinute());
        System.out.println(localTime.getSecond());
        System.out.println(localTime.MIDNIGHT);
        System.out.println(localTime.NOON);
    }
}
